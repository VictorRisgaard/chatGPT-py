from comment_section_app import CommentSectionApp

def main():
    app = CommentSectionApp()
    app.mainloop()

if __name__ == "__main__":
    main()
